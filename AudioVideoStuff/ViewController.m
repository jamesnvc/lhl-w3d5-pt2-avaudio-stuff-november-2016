//
//  ViewController.m
//  AudioVideoStuff
//
//  Created by James Cash on 18-11-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface ViewController ()
@property NSURL *savedAudioURL;
@property AVAudioSession *session;
@property AVAudioRecorder *recorder;
@property AVAudioPlayer *player;
@end

@implementation ViewController

- (IBAction)playRecording:(UIButton*)sender {
    if (self.recorder.isRecording) {
        return;
    }

    if (self.player.isPlaying) {
        [self.player stop];
        [sender setTitle:@"Play" forState:UIControlStateNormal];
        return;
    }

    [sender setTitle:@"Stop" forState:UIControlStateNormal];
    NSError *err;
    self.player = [[AVAudioPlayer alloc]
                   initWithContentsOfURL:self.savedAudioURL
                   error:&err];
    [self.session setCategory:AVAudioSessionCategoryPlayback error:&err];
    [self.player play];
}

- (IBAction)startRecording:(UIButton*)sender {
    if (self.recorder.isRecording) {
        [self.recorder stop];
        [sender setTitle:@"Record" forState:UIControlStateNormal];
        return;
    }
    [self.player stop];

    if (self.session.recordPermission != AVAudioSessionRecordPermissionGranted) {
        [self.session requestRecordPermission:^(BOOL granted) {
            if (granted) {
                [self startRecording:sender];
            }
        }];
        return;
    }

    [sender setTitle:@"Stop" forState:UIControlStateNormal];
    NSError *err = nil;
    self.recorder = [[AVAudioRecorder alloc]
                     initWithURL:self.savedAudioURL
                     settings:@{AVSampleRateKey: @(44100),
                                AVNumberOfChannelsKey: @(2),
                                AVFormatIDKey: @(kAudioFormatMPEG4AAC)}
                     error:&err];
    if (err != nil) {
        NSLog(@"Something went wrong: %@", err.localizedDescription);
        return;
    }
    [self.session setCategory:AVAudioSessionCategoryRecord error:&err];
    if (err != nil) {
        NSLog(@"Couldn't set category: %@", err.localizedDescription);
    }
    [self.recorder record];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.session = [AVAudioSession sharedInstance];
    NSString * documentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    self.savedAudioURL = [NSURL fileURLWithPathComponents:@[documentsDir, @"recorded.m4a"]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
